<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vnecoms\Setup\Module;

use Magento\Framework\App\ResourceConnection;
use Vnecoms\Setup\Module\Setup\ResourceConfig;
use Zend\ServiceManager\ServiceLocatorInterface;

class ResourceFactory
{
    /**
     * Zend Framework's service locator
     *
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * Constructor
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * @param \Magento\Framework\App\DeploymentConfig $deploymentConfig
     * @return Resource
     */
    public function create(\Magento\Framework\App\DeploymentConfig $deploymentConfig)
    {
        $connectionFactory = $this->serviceLocator->get('Vnecoms\Setup\Module\ConnectionFactory');
        $resource = new ResourceConnection(
            new ResourceConfig(),
            $connectionFactory,
            $deploymentConfig
        );
        return $resource;
    }
}
