<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

use Vnecoms\Setup\Mvc\Bootstrap\InitParamListener;

return [
    'modules' => [
        'Vnecoms\Setup',
    ],
    'module_listener_options' => [
        'module_paths' => [
            __DIR__ . '/../src',
        ],
        'config_glob_paths' => [
            __DIR__ . '/autoload/{,*.}{global,local}.php',
        ],
    ],
    'listeners' => ['Vnecoms\Setup\Mvc\Bootstrap\InitParamListener'],
    'service_manager' => [
        'factories' => [
            InitParamListener::BOOTSTRAP_PARAM => 'Vnecoms\Setup\Mvc\Bootstrap\InitParamListener',
        ],
    ],
];
