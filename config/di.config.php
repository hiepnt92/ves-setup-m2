<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

return [
    'di' => [
        'allowed_controllers' => [
            'Vnecoms\Setup\Controller\Index',
            'Vnecoms\Setup\Controller\LandingInstaller',
            'Vnecoms\Setup\Controller\LandingUpdater',
            'Vnecoms\Setup\Controller\CreateBackup',
            'Vnecoms\Setup\Controller\CompleteBackup',
            'Vnecoms\Setup\Controller\Navigation',
            'Vnecoms\Setup\Controller\Home',
            'Vnecoms\Setup\Controller\SelectVersion',
            'Vnecoms\Setup\Controller\License',
            'Vnecoms\Setup\Controller\ReadinessCheckInstaller',
            'Vnecoms\Setup\Controller\ReadinessCheckUpdater',
            'Vnecoms\Setup\Controller\Environment',
            'Vnecoms\Setup\Controller\DependencyCheck',
            'Vnecoms\Setup\Controller\DatabaseCheck',
            'Vnecoms\Setup\Controller\ValidateAdminCredentials',
            'Vnecoms\Setup\Controller\AddDatabase',
            'Vnecoms\Setup\Controller\WebConfiguration',
            'Vnecoms\Setup\Controller\CustomizeYourStore',
            'Vnecoms\Setup\Controller\CreateAdminAccount',
            'Vnecoms\Setup\Controller\Install',
            'Vnecoms\Setup\Controller\Success',
            'Vnecoms\Setup\Controller\Modules',
            'Vnecoms\Setup\Controller\ComponentGrid',
            'Vnecoms\Setup\Controller\StartUpdater',
            'Vnecoms\Setup\Controller\UpdaterSuccess',
            'Vnecoms\Setup\Controller\BackupActionItems',
            'Vnecoms\Setup\Controller\Maintenance',
            'Vnecoms\Setup\Controller\OtherComponentsGrid',
            'Vnecoms\Setup\Controller\DataOption',
            'Vnecoms\Setup\Controller\Marketplace',
            'Vnecoms\Setup\Controller\SystemConfig',
            'Vnecoms\Setup\Controller\InstallExtensionGrid',
            'Vnecoms\Setup\Controller\MarketplaceCredentials',
            'Vnecoms\Setup\Controller\Session'
        ],
        'instance' => [
            'preference' => [
                'Zend\EventManager\EventManagerInterface' => 'EventManager',
                'Zend\ServiceManager\ServiceLocatorInterface' => 'ServiceManager',
                'Magento\Framework\DB\LoggerInterface' => 'Magento\Framework\DB\Logger\Quiet',
                'Magento\Framework\Locale\ConfigInterface' => 'Magento\Framework\Locale\Config',
                'Magento\Framework\Filesystem\DriverInterface' => 'Magento\Framework\Filesystem\Driver\File',
                'Magento\Framework\Component\ComponentRegistrarInterface' =>
                    'Magento\Framework\Component\ComponentRegistrar',
            ],
        ],
    ],
];
